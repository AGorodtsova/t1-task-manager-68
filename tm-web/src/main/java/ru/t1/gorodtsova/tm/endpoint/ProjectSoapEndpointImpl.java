package ru.t1.gorodtsova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.gorodtsova.tm.api.service.IProjectService;
import ru.t1.gorodtsova.tm.soap.*;

@Endpoint
public class ProjectSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public final static String NAMESPACE = "http://tm.gorodtsova.t1.ru/soap";

    @Autowired
    private IProjectService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    protected ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) {
        return new ProjectFindAllResponse(projectService.findAll());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    protected ProjectFindByIdResponse findById(@RequestPayload final ProjectFindByIdRequest request) {
        return new ProjectFindByIdResponse(projectService.findById(request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    protected ProjectSaveResponse save(@RequestPayload final ProjectSaveRequest request) {
        projectService.save(request.getProject());
        return new ProjectSaveResponse(request.getProject());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    protected ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) {
        projectService.removeById(request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    protected ProjectDeleteResponse delete(@RequestPayload final ProjectDeleteRequest request) {
        projectService.remove(request.getProject());
        return new ProjectDeleteResponse();
    }

}
