package ru.t1.gorodtsova.tm.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.gorodtsova.tm.model.Task;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskDeleteRequest")
public class TaskDeleteRequest {

    @XmlElement(required = true)
    protected Task task;

}
