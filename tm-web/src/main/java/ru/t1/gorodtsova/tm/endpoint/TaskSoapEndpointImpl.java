package ru.t1.gorodtsova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.gorodtsova.tm.api.service.ITaskService;
import ru.t1.gorodtsova.tm.soap.*;

@Endpoint
public class TaskSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public final static String NAMESPACE = "http://tm.gorodtsova.t1.ru/soap";

    @Autowired
    private ITaskService taskService;

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    private TaskFindAllResponse findAll(@RequestPayload final TaskFindAllRequest request) {
        return new TaskFindAllResponse(taskService.findAll());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    private TaskFindByIdResponse findById(@RequestPayload final TaskFindByIdRequest request) {
        return new TaskFindByIdResponse(taskService.findById(request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    private TaskSaveResponse save(@RequestPayload final TaskSaveRequest request) {
        taskService.save(request.getTask());
        return new TaskSaveResponse(request.getTask());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(@RequestPayload final TaskDeleteByIdRequest request) {
        taskService.removeById(request.getId());
        return new TaskDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    private TaskDeleteResponse delete(@RequestPayload final TaskDeleteRequest request) {
        taskService.remove(request.getTask());
        return new TaskDeleteResponse();
    }

}
