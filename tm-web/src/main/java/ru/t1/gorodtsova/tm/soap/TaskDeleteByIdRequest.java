package ru.t1.gorodtsova.tm.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskDeleteByIdRequest")
public class TaskDeleteByIdRequest {

    @XmlElement(required = true)
    protected String id;

}
