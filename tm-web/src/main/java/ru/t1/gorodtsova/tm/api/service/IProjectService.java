package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void create();

    void save(@Nullable Project model);

    void clear();

    @Nullable
    List<Project> findAll();

    @Nullable
    Project findById(@Nullable String id);

    void remove(@Nullable Project model);

    void removeById(@Nullable String id);

}
