package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create();

    void save(@Nullable Task model);

    void clear();

    @Nullable
    List<Task> findAll();

    @Nullable
    Task findById(@Nullable String id);

    void remove(@Nullable Task model);

    void removeById(@Nullable String id);

}
