package ru.t1.gorodtsova.tm.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.gorodtsova.tm.model.Task;

import javax.xml.bind.annotation.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskSaveResponse")
public class TaskSaveResponse {

    @XmlElement(required = true)
    protected Task task;

}
