package ru.t1.gorodtsova.tm.soap;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public ProjectFindAllRequest createProjectFindAllRequest() {
        return new ProjectFindAllRequest();
    }

    public ProjectFindAllResponse createProjectFindAllResponse() {
        return new ProjectFindAllResponse();
    }

    public ProjectFindByIdRequest createProjectFindByIdRequest() {
        return new ProjectFindByIdRequest();
    }

    public ProjectFindByIdResponse createProjectFindByIdResponse() {
        return new ProjectFindByIdResponse();
    }

    public ProjectSaveRequest createProjectSaveRequest() {
        return new ProjectSaveRequest();
    }

    public ProjectSaveResponse createProjectSaveResponse() {
        return new ProjectSaveResponse();
    }

    public ProjectDeleteByIdRequest createProjectDeleteByIdRequest() {
        return new ProjectDeleteByIdRequest();
    }

    public ProjectDeleteByIdResponse createProjectDeleteByIdResponse() {
        return new ProjectDeleteByIdResponse();
    }

    public ProjectDeleteRequest createProjectDeleteRequest() {
        return new ProjectDeleteRequest();
    }

    public ProjectDeleteResponse createProjectDeleteResponse() {
        return new ProjectDeleteResponse();
    }

    public TaskFindAllRequest createTaskFindAllRequest() {
        return new TaskFindAllRequest();
    }

    public TaskFindAllResponse createTaskFindAllResponse() {
        return new TaskFindAllResponse();
    }

    public TaskFindByIdRequest createTaskFindByIdRequest() {
        return new TaskFindByIdRequest();
    }

    public TaskFindByIdResponse createTaskFindByIdResponse() {
        return new TaskFindByIdResponse();
    }

    public TaskSaveRequest createTaskSaveRequest() {
        return new TaskSaveRequest();
    }

    public TaskSaveResponse createTaskSaveResponse() {
        return new TaskSaveResponse();
    }

    public TaskDeleteByIdRequest createTaskDeleteByIdRequest() {
        return new TaskDeleteByIdRequest();
    }

    public TaskDeleteByIdResponse createTaskDeleteByIdResponse() {
        return new TaskDeleteByIdResponse();
    }

    public TaskDeleteRequest createTaskDeleteRequest() {
        return new TaskDeleteRequest();
    }

    public TaskDeleteResponse createTaskDeleteResponse() {
        return new TaskDeleteResponse();
    }

}
