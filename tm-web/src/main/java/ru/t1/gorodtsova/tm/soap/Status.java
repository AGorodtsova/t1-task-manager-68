package ru.t1.gorodtsova.tm.soap;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "status")
public enum Status {

    NOT_STARTED,
    IN_PROGRESS,
    COMPLETED;

    public static Status fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
