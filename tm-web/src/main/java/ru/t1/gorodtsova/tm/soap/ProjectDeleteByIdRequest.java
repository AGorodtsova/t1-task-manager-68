package ru.t1.gorodtsova.tm.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "projectDeleteByIdRequest")
public class ProjectDeleteByIdRequest {

    @XmlElement(required = true)
    protected String id;

}
