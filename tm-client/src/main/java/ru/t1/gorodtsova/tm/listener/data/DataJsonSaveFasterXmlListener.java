package ru.t1.gorodtsova.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.domain.DataJsonSaveFasterXmlRequest;
import ru.t1.gorodtsova.tm.event.ConsoleEvent;

@Component
public final class DataJsonSaveFasterXmlListener extends AbstractDataListener {

    @NotNull
    private final String DESCRIPTION = "Save data in json file";

    @NotNull
    private final String NAME = "data-save-json";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@dataJsonSaveFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataJsonSaveFasterXmlRequest request = new DataJsonSaveFasterXmlRequest(getToken());
        domainEndpoint.saveDataJsonFasterXml(request);
    }

}
